﻿using System.Windows;
using System.Windows.Controls;

namespace MyControls
{
    public class FoldableGroupBoxControl : HeaderedContentControl
    {
        static FoldableGroupBoxControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FoldableGroupBoxControl), new FrameworkPropertyMetadata(typeof(FoldableGroupBoxControl)));
        }

        public object GroupTitle
        {
            get { return (object)GetValue(GroupTitleProperty); }
            set { SetValue(GroupTitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for GroupTitle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GroupTitleProperty =
            DependencyProperty.Register("GroupTitle", typeof(object), typeof(FoldableGroupBoxControl), new UIPropertyMetadata(null));
    }
}
